#include "gtest/gtest.h"
#include "Polinom.h"
#include "DatList.h"
#include "HeadRing.h"

TEST(TPolinom, can_create_polynom)
{
	int monom[][2] = { { 1, 0 } };

	EXPECT_NO_THROW(TPolinom Res(monom, 1););
}

TEST(TPolinom, copied_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2(P1);

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolinom, copied_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2(P1);

	EXPECT_NE(&P1, &P2);
}

TEST(TPolinom, assign_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2;

	P2 = P1;

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolinom, assign_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2;

	P2 = P1;

	EXPECT_NE(&P1, &P2);
}

TEST(TPolinom, compare_is_correct)
{
	int monom1[][2] = { { 5, 321 },{ 7, 213 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 7, 213 } };
	TPolinom P1(monom1, 3);
	TPolinom P2(monom2, 2);

	EXPECT_FALSE(P1 == P2);
}

TEST(TPolinom, can_multiple_assign_polynoms)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2;
	TPolinom P3;

	P3 = P2 = P1;

	EXPECT_TRUE(P1 == P3);
}

TEST(TPolinom, can_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };
	int monom3[][2] = { { 10, 321 },{ 7, 223 },{ 17, 213 },{ 2, 153 },{ 1, 0 } };
	TPolinom P1(monom1, 3);
	TPolinom P2(monom2, 3);
	TPolinom res;

	res = P1 + P2;

	TPolinom expect(monom3, 5);

	EXPECT_TRUE(res == expect);
}

TEST(TPolinom, can_multiple_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };
	int monom3[][2] = { { -10, 321 },{ 4, 313 },{ 5, 0 } };
	int monom4[][2] = { { 4, 313 },{ 7, 223 },{ 17, 213 },{ 2, 153 },{ 6, 0 } };
	TPolinom P1(monom1, 3);
	TPolinom P2(monom2, 3);
	TPolinom P3(monom3, 3);
	TPolinom res;

	res = P1 + P2 + P3;

	TPolinom expect(monom4, 5);

	EXPECT_TRUE(res == expect);
}