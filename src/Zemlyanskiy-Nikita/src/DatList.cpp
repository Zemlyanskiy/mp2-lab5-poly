#pragma once
#include "DatList.h"

//protected//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) // getting of a new link
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)	// removal of the link
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;		// removal of a value in the link
		delete pLink;					// removal of the link
	}
}

//public/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

TDatList::TDatList()
{
	ListLen = 0;
	pFirst = pCurrLink = pLast = nullptr;
	Reset();
}

int TDatList::Reset(void) // set in the beginning of the list 
{
	pPrevLink = pStop;  // pCurrLink has to be equal to beginning of the all list
	if (!IsEmpty())		// but the list can be not empty
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
	else				// and can be empty
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
	return 0;
}

//access/////////////////////////////////////////////////////////////////////////

PTDatValue TDatList::GetDatValue(TLinkPos mode) const // get a value of a link
{
	PTDatLink tmp = nullptr;
	switch (mode)
	{
	case FIRST:
		tmp = pFirst;
		break;
	case LAST:
		tmp = pLast;
		break;
	default:
		tmp = pCurrLink;
		break;

	}
	if (tmp != nullptr)
		return tmp->TDatLink::GetDatValue();
	else
		return nullptr;
}

//navigation/////////////////////////////////////////////////////////////////////

int TDatList::GoNext(void) // shift to the right the current link
{
	if (!IsListEnded())
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

int TDatList::SetCurrentPos(int pos) // set the current link
{
	if (pos <= GetListLength())
	{
		Reset();
		for (int i = 0; i < pos; i++, GoNext()) {}
	}
	else
		throw Error_1000;
	return 0;
}

int TDatList::GetCurrentPos(void) const // get the CurrPos 
{
	return CurrPos;
}

int TDatList::IsListEnded(void) const  // the list ended or not
{
	return pCurrLink == pStop;
}

//insert/////////////////////////////////////////////////////////////////////////

void TDatList::InsFirst(PTDatValue pVal) // insert before the first link
{
	PTDatLink tmp = GetLink(pVal, pFirst);
	if (tmp) // if it isn't equal nullptr
	{
		tmp->SetNextLink(pFirst);
		pFirst = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pLast = tmp;
			Reset();
		}
		else  if (CurrPos == 0)
			pCurrLink = tmp;
		else
			CurrPos++;
	}
}
void TDatList::InsLast(PTDatValue pVal) // insert the last
{
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp)		// if it isn't equal nullptr
	{
		if (pLast)	// if it isn't equal nullptr
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;
	}
}

void TDatList::InsCurrent(PTDatValue pVal) // insert before the current link
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else
	{
		if (IsListEnded())
			InsLast(pVal);
		else
		{
			PTDatLink tmp = GetLink(pVal, pCurrLink);
			if (tmp) // if it isn't equal nullptr
			{
				pPrevLink->SetNextLink(tmp);
				tmp->SetNextLink(pCurrLink);
				ListLen++;
				pCurrLink = tmp;
			}
		}
	}
}

//delete & removal///////////////////////////////////////////////////////////////

void TDatList::DelFirst(void) // remove the first link
{
	if (!IsEmpty())
	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;

		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 1)	// if the first link was previous for the current link
			pPrevLink = pStop;
		else if (CurrPos == 0)	// if the first link was the current link
			pCurrLink = pFirst;
		if (CurrPos)			// after removing of the first link the current link has to be 1 less
			CurrPos--;
	}
	else
		throw Error_1001;
}
void TDatList::DelCurrent(void) // remove the current link
{
	if (pCurrLink)								// if it isn't equal nullptr
	{
		if ((pCurrLink == pFirst) || IsEmpty())	// remove the first link was higher
			DelFirst();
		else
		{
			PTDatLink tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(tmp);
			ListLen--;

			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
	else
		throw Error_1001;
}
void TDatList::DelList(void) // remove the all list
{
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}