#include "Polinom.h"
#include <iostream>

using namespace std;

void main()
{
	int ms1[][2] = { { 1,123 },{ 2,332 },{ 3,151 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom p(ms1, mn1);
	cout << "polinom #1" << endl << p << endl;
	int ms2[][2] = { { 1,123 },{ 3,332 },{ 3,151 },{ 4,514 } };
	int mn2 = sizeof(ms2) / (2 * sizeof(int));
	TPolinom q(ms2, mn2);
	cout << "polinom #2" << endl << q << endl;

	if (p == q)
		cout << "polinom #1 = polinom #2" << endl;
	else
		cout << "polinom #1 != polinom #2" << endl;
	TPolinom r = p + q;
	cout << "p + q = " << endl << r << endl;
	cout << "result r = p + q, where x = 1, y = 1, z = 2: " << endl << r.CalculatePoly(1, 1, 2) << endl;
	system("pause");
}
