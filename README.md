# Лабораторная работа №5. Полиномы
## Введение
В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:
+	ввод полинома 
+	организация хранения полинома
+	удаление введенного ранее полинома;
+	копирование полинома;
+	сложение двух полиномов;
+	вычисление значения полинома при заданных значениях переменных;
+	вывод.
Состав реализуемых операций над полиномами может быть расширен при постановке задания лабораторной работы.
Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:
+	поддержка понятия текущего звена;
+	вставка звеньев в начало, после текущей позиции и в конец списков;
+	удаление звеньев в начале и в текущей позиции списков;
+	организация последовательного доступа к звеньям списка (итератор).
В ходе выполнения лабораторной работы должно быть выполнено сопоставление разработанных средств работы со списками с возможностями работы со списками в библиотеке STL.
Важной частью лабораторной работы должна являться разработка диалоговой управляющей программы с наглядным визуальным интерфейсом, который обеспечивает возможности создавать полиномы, выполнять реализованные операции обработки полиномов, демонстрировать вид имеющихся полиномов. 
#### Условия и ограничения
При выполнении лабораторной работы можно использовать следующие основные предположения:
1. Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
2. Степени переменных полиномов не могут превышать значения 9.
3. Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).
#### Структуры данных и структуры хранения
Для организации быстрого доступа может быть использовано упорядоченное хранение мономов. Для задания порядка следования можно принять лексикографическое упорядочивание по степеням переменных, при котором мономы упорядочиваются по степеням первой переменной, потом по второй переменной, и только затем по третьей переменной. В общем виде это правило можно записать как соотношение: моном X^A1 Y^B1 Z^C1 предшествует моному X^A2 Y^B2 Z^C2 тогда и только тогда, если
![](https://pp.userapi.com/c626625/v626625362/6a786/SQ2oSQVanzE.jpg)
![](https://pp.userapi.com/c626625/v626625362/6a78d/hWBLUy76sFc.jpg)
В результате наиболее эффективным способом организации структуры хранения полиномов являются линейный (односвязный) список.
#### Структура проекта
![](https://pp.userapi.com/c626625/v626625362/6a794/TYgdmhfrk3I.jpg)
#### Спецификации классов
- **Абстрактный класс объектов-значений TDatValue (файл DatValue.h):**
``` c++
#pragma once
class TDatValue
{
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
typedef TDatValue *PTDatValue;
```
- **Класс моном TMonom (файл Monom.h):**
``` c++
#pragma once
#include "DatValue.h"

class TMonom;
typedef TMonom *PTMonom;

#include <iostream>
using namespace std;

class TMonom : public TDatValue
{
protected:
	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)
public:
	TMonom(int cval = 1, int ival = 0)
	{
		Coeff = cval; Index = ival;
	};
	// изготовить копию
	virtual TDatValue * GetCopy() {return new TMonom(Coeff, Index);} 
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }
	TMonom& operator=(const TMonom &tm) {
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	int operator==(const TMonom &tm) {
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator<(const TMonom &tm) {
		return Index<tm.Index;
	}
	friend ostream& operator<<(ostream &os, TMonom &tm) {
		os << tm.Coeff << " " << tm.Index;
		return os;
	}
	friend class TPolinom;
};
```
- **Базовый класс для звеньев (элементов) списка TRootLink
(файл RootLink.h):**
``` c++
#pragma once
#include <iostream>
#include "DatValue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink 
{
protected:
	PTRootLink  pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = nullptr) { pNext = pN; }
	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink) {
		PTRootLink p = pNext;  
		pNext = pLink;
		if (pLink != nullptr) 
		    pLink->pNext = p;
	}
	virtual void SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};
```
- **Класс для звеньев (элементов) списка с указателем на 
объект-значение TDatLink (файл DatLink.h):**
``` c++
#pragma once
#include "DatValue.h"
#include "RootLink.h"
class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink 
{
protected:
	PTDatValue pValue;  // указатель на объект значения
public:
	TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) :TRootLink(pN)
	{
		pValue = pVal;
	}
	void SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};
```
- **Класс линейных списков (файл DatList.h):**
``` c++
#pragma once
#include "tdatacom.h"
#include "DatLink.h"

#define Error_1000 1000  // SetCurrentPos( pos ), где pos больше, чем длина списка
#define Error_1001 1001  // Удаление ссылки не может выполняться, если длина списка равна nullptr

enum TLinkPos {FIRST,CURRENT,LAST};

class TDatList:public TDataCom
{
protected:
	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка 
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке
protected:  // методы
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);   // удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }
	// доступ
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty()  const { return pFirst == pStop; } // список пуст ?
	int GetListLength()    const { return ListLen; }       // к-во звеньев
														   // навигация
	int SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual int Reset(void);             // установить на начало списка
	virtual int IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)
										 // вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // перед первым
	virtual void InsLast(PTDatValue pVal = nullptr); // вставить последним 
	virtual void InsCurrent(PTDatValue pVal = nullptr); // перед текущим 
													 // удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список
};
typedef TDatList *PTDatList;
```
- **Реализация списка (файл DatList.cpp)**
``` c++
#pragma once
#include "DatList.h"

//protected методы

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) // Получение нового звена
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)	// удаление звена
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;		// удаление значения звена
		delete pLink;					// удаление звена
	}
}

//public методы

TDatList::TDatList()
{
	ListLen = 0;
	pFirst = pCurrLink = pLast = nullptr;
	Reset();
}

int TDatList::Reset(void) // установить на начало списка
{
    // pCurrLink должен быть равен началу всего списка
	pPrevLink = pStop;  
	if (!IsEmpty())		
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
	else				
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
	return 0;
}

//методы доступа

PTDatValue TDatList::GetDatValue(TLinkPos mode) const 
// получить значение звена
{
	PTDatLink tmp = nullptr;
	switch (mode)
	{
	case FIRST:
		tmp = pFirst;
		break;
	case LAST:
		tmp = pLast;
		break;
	default:
		tmp = pCurrLink;
		break;

	}
	if (tmp != nullptr)
		return tmp->TDatLink::GetDatValue();
	else
		return nullptr;
}

//методы навигации

int TDatList::GoNext(void) 
// сдвиг вправо текущего звена
{
	if (!IsListEnded())
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

int TDatList::SetCurrentPos(int pos) 
// установить текущее звено
{
	if (pos <= GetListLength())
	{
		Reset();
		for (int i = 0; i < pos; i++, GoNext()) {}
	}
	else
		throw Error_1000;
	return 0;
}

int TDatList::GetCurrentPos(void) const
// получить номер текущего звена
{
	return CurrPos;
}

int TDatList::IsListEnded(void) const  
// список завершен?
{
	return pCurrLink == pStop;
}

//методы вставки звеньев

void TDatList::InsFirst(PTDatValue pVal) 
// вставить перед первым
{
	PTDatLink tmp = GetLink(pVal, pFirst);
	if (tmp)
	{
		tmp->SetNextLink(pFirst);
		pFirst = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pLast = tmp;
			Reset();
		}
		else  if (CurrPos == 0)
			pCurrLink = tmp;
		else
			CurrPos++;
	}
}
void TDatList::InsLast(PTDatValue pVal)
// вставить последним
{
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp)		
	{
		if (pLast)	
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;
	}
}

void TDatList::InsCurrent(PTDatValue pVal) 
// вставить перед текущим
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else
	{
		if (IsListEnded())
			InsLast(pVal);
		else
		{
			PTDatLink tmp = GetLink(pVal, pCurrLink);
			if (tmp) 
			{
				pPrevLink->SetNextLink(tmp);
				tmp->SetNextLink(pCurrLink);
				ListLen++;
				pCurrLink = tmp;
			}
		}
	}
}

//методы удаления звеньев

void TDatList::DelFirst(void) 
// удалить первое звено
{
	if (!IsEmpty())
	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;

		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 1)	
        // если первое звено было перед текущим
			pPrevLink = pStop;
		else if (CurrPos == 0)	
        // если первое звено было текущим
			pCurrLink = pFirst;
		if (CurrPos)			
        // после удаления первого звена текущее становится первым
			CurrPos--;
	}
	else
		throw Error_1001;
}
void TDatList::DelCurrent(void) 
// удалить текущее звено
{
	if (pCurrLink)								
	{
		if ((pCurrLink == pFirst) || IsEmpty())
			DelFirst();
		else
		{
			PTDatLink tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(tmp);
			ListLen--;

			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
	else
		throw Error_1001;
}
void TDatList::DelList(void) 
// удалить весь список
{
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}
```
- **Класс циклических списков с заголовком THeadRing 
(файл HeadRing.h):**
``` c++
#pragma once
#include "DatList.h"
class THeadRing : public TDatList 
{
protected:
	PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка
												   // удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено
};
```
- **Реализация класса циклических списков
(файл HeadRing.cpp)**
``` c++
#include "HeadRing.h"

THeadRing::THeadRing() :TDatList()
{
	InsLast();
	pHead = pFirst;
	ListLen = 0;
	pStop = pHead;
	Reset();
	pFirst->SetNextLink(pFirst);
}

THeadRing::~THeadRing()
{
	TDatList::DelList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal)
//вставить после заголовка
{
	TDatList::InsFirst(pVal);
}

void THeadRing::DelFirst(void)
//удалить первое звено
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
```
- **Класс полином TPolinom (файл Polinom.h):**
``` c++
#pragma once
#include "HeadRing.h"
#include "Monom.h"

#include <iostream>
using namespace std;

class TPolinom : public THeadRing 
{
public:
	TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор
												  // полинома из массива «коэффициент-индекс»
	TPolinom(TPolinom &q);      // конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q); // сравнение полиномов
	bool operator!=(TPolinom &q); // сравнение полиномов
	double CalculatePoly(int x = 0, int y = 0, int z = 0);
	friend ostream& operator<<(ostream &os, TPolinom &q);
	// печать полинома
};
```
- **Реализация класса полином(файл Polinom.cpp)**
``` c++
#pragma once
#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (int i = 0; i < km; i++)
	{
		pMonom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(pMonom);
	}
}

TPolinom & TPolinom::operator+(TPolinom&q)
//сложение полиномов
{
	PTMonom pm, qm, rm;
	Reset();
	q.Reset();
	while (1)
	{
		pm = GetMonom();
		qm = q.GetMonom();
		if (pm->Index < qm->Index)
			//моном pm младше монома qm =>добавление монома qm в полином pm
		{
			rm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(rm);
			q.GoNext();
		}
		else if (pm->Index > qm->Index)
			GoNext();
		else//индексы мономов равны(но это могут быть головы!)
		{
			if (pm->Index == -1)
				break;
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0)
			{
				GoNext();
				q.GoNext();
			}
			else
			//удаление монома с нулевым коэффициентом
			{
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	int qPos = q.GetCurrentPos();
	for (q.Reset(); !q.IsListEnded(); q.GoNext() )
	{
		pMonom = q.GetMonom();
		InsLast(pMonom->GetCopy());
	}
	q.SetCurrentPos(qPos);
}

TPolinom & TPolinom::operator=(TPolinom&q)
//присваивание
{
	DelList();
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		PTMonom pMonom = q.GetMonom();
		InsLast(pMonom->GetCopy());//удалить pMonom
	}
	return *this;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
		cout << *q.GetMonom() << endl;
	return os;
}

bool TPolinom::operator==(TPolinom &q) // сравнение полиномов
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		PTMonom Mon1, Mon2;
		Reset(); 
		q.Reset();
		while (!IsListEnded())
		{
			Mon1 = GetMonom();
			Mon2 = GetMonom();
			if (*Mon1 == *Mon2)
			{
				GoNext(); 
				q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

bool TPolinom::operator!=(TPolinom &q) // сравнение полиномов
{
	return !(*this == q);
}

double TPolinom::CalculatePoly(int x, int y, int z)
{
	double res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
		}
	}
	return res;
}
```
#### Тестирование списка
- **Реализация тестов**
``` c++
#include "gtest/gtest.h"
#include "DatList.h"
#include "Monom.h"

TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList l1);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList l1;
	ASSERT_TRUE(l1.IsEmpty());
}

TEST(TDatList, can_put_link_in_list)
{
	TDatList l1;
	PTMonom m1;
	m1 = new TMonom(4, 123);
	ASSERT_NO_THROW(l1.InsLast(m1));
}

TEST(TDatList, list_with_links_is_not_empty)
{
	TDatList l1;
	PTMonom m1, m2;
	m1 = new TMonom(4, 123);
	m2 = new TMonom(8, 456);
	l1.InsLast(m1);
	l1.InsLast(m2);
	ASSERT_TRUE((!l1.IsEmpty()) && (l1.GetListLength() == 2));
}

TEST(TDatList, can_get_current_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	EXPECT_EQ(l1.GetCurrentPos(), 0);
}

TEST(TDatList, can_set_current_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	EXPECT_EQ(l1.GetCurrentPos(), 4);
}

TEST(TDatList, can_go_next_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.GoNext();
	EXPECT_EQ(l1.GetCurrentPos(), 5);
}

TEST(TDatList, can_reset_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.Reset();
	EXPECT_EQ(l1.GetCurrentPos(), 0);
}

TEST(TDatList, ended_list_is_ended)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.GoNext();
	EXPECT_TRUE(l1.IsListEnded());
}

TEST(TDatList, can_get_link_from_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(pVal->GetIndex() + pVal->GetCoeff(), 560);
}

TEST(TDatList, can_put_link_before_the_first)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsFirst(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_after_the_last)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsLast(pVal);
	l1.SetCurrentPos(5);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_before_the_current)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(2);
	pVal = new TMonom(6, 666);
	l1.InsCurrent(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_delete_first_link)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	temp = (PTMonom)l1.GetDatValue();
	l1.DelFirst();
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((temp->GetIndex() + temp->GetCoeff() != pVal->GetIndex() 
	            + pVal->GetCoeff()) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_current_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(3);
	l1.DelCurrent();
	l1.SetCurrentPos(3);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((pVal->GetIndex() + pVal->GetCoeff() == 448)
	            && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.DelList();
	EXPECT_TRUE((l1.IsEmpty()) && (l1.GetListLength() == 0) 
	            && (l1.GetDatValue() == nullptr));
}
```
- **Успешное прохождение тестов для списка**
![](https://pp.userapi.com/c626625/v626625362/6a7b9/WD6eOHYX-AE.jpg)
#### Тестирование полиномов
- **Реализация тестов**
``` c++
#include "gtest/gtest.h"
#include "Polinom.h"
#include "DatList.h"
#include "HeadRing.h"

TEST(TPolinom, can_create_polynom)
{
	int monom[][2] = { { 1, 0 } };

	EXPECT_NO_THROW(TPolinom Res(monom, 1););
}

TEST(TPolinom, copied_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2(P1);

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolinom, copied_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2(P1);

	EXPECT_NE(&P1, &P2);
}

TEST(TPolinom, assign_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2;

	P2 = P1;

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolinom, assign_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2;

	P2 = P1;

	EXPECT_NE(&P1, &P2);
}

TEST(TPolinom, compare_is_correct)
{
	int monom1[][2] = { { 5, 321 },{ 7, 213 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 7, 213 } };
	TPolinom P1(monom1, 3);
	TPolinom P2(monom2, 2);

	EXPECT_FALSE(P1 == P2);
}

TEST(TPolinom, can_multiple_assign_polynoms)
{
	int monom[][2] = { { 1, 0 } };
	TPolinom P1(monom, 1);
	TPolinom P2;
	TPolinom P3;

	P3 = P2 = P1;

	EXPECT_TRUE(P1 == P3);
}

TEST(TPolinom, can_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };
	int monom3[][2] = { { 10, 321 },{ 7, 223 },{ 17, 213 },{ 2, 153 },{ 1, 0 } };
	TPolinom P1(monom1, 3);
	TPolinom P2(monom2, 3);
	TPolinom res;

	res = P1 + P2;

	TPolinom expect(monom3, 5);

	EXPECT_TRUE(res == expect);
}

TEST(TPolinom, can_multiple_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };
	int monom3[][2] = { { -10, 321 },{ 4, 313 },{ 5, 0 } };
	int monom4[][2] = { { 4, 313 },{ 7, 223 },{ 17, 213 },{ 2, 153 },{ 6, 0 } };
	TPolinom P1(monom1, 3);
	TPolinom P2(monom2, 3);
	TPolinom P3(monom3, 3);
	TPolinom res;

	res = P1 + P2 + P3;

	TPolinom expect(monom4, 5);

	EXPECT_TRUE(res == expect);
}
```
- **Успешное прохождение тестов для полиномов**
![](https://pp.userapi.com/c626625/v626625362/6a7c0/ZnaclZFarJo.jpg)
- **Реализация тестового приложения**
``` c++
#include "Polinom.h"
#include <iostream>

using namespace std;

void main()
{
	int ms1[][2] = { { 1,123 },{ 2,332 },{ 3,151 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom p(ms1, mn1);
	cout << "polinom #1" << endl << p << endl;
	int ms2[][2] = { { 1,123 },{ 3,332 },{ 3,151 },{ 4,514 } };
	int mn2 = sizeof(ms2) / (2 * sizeof(int));
	TPolinom q(ms2, mn2);
	cout << "polinom #2" << endl << q << endl;

	if (p == q)
		cout << "polinom #1 = polinom #2" << endl;
	else
		cout << "polinom #1 != polinom #2" << endl;
	TPolinom r = p + q;
	cout << "p + q = " << endl << r << endl;
	cout << "result r = p + q, where x = 1, y = 1, z = 2: " << endl << r.CalculatePoly(1, 1, 2) << endl;
	system("pause");
}
```
- **Успешная работа тестирующего приложения**
![](https://pp.userapi.com/c626625/v626625362/6a7c7/daKAIMq-bh4.jpg)
### Выводы:
**Выполнив данную работу мы научились:**
- Реализации односвязнного списка
- Представлению полиномов и выполнению некоторых операций над ними

